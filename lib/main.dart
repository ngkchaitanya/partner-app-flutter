import 'package:flutter/material.dart';
import 'login_page.dart';
import 'home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Partner Sample 2',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: MaterialColor(CompanyColors.pink[50].value, CompanyColors.pink),
        fontFamily: 'Roboto',
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}

TextStyle style = TextStyle(fontFamily: 'Roboto', fontSize: 20.0);

class CompanyColors {
  CompanyColors._(); // this basically makes it so you can instantiate this class
  static const Map<int, Color> pink = const <int, Color>{
    50: const Color(0xffed3e72),
    100: const Color(0xffed3e72),
    200: const Color(0xffed3e72),
    300: const Color(0xffed3e72),
    400: const Color(0xffed3e72),
    500: const Color(0xffed3e72),
    600: const Color(0xffed3e72),
    700: const Color(0xffed3e72),
    800: const Color(0xffed3e72),
    900: const Color(0xffed3e72)
  };
}
