import 'package:flutter/material.dart';
import 'package:login/home_page.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final appLoginTitleImg = SizedBox(
      height: 50,
      child: Image.asset(
        "assets/images/logo.png",
        fit: BoxFit.fitHeight,
      ),
    );

    final appLoginTitleText = SizedBox(
        child: Text('Partner',
        textAlign: TextAlign.center,
            style: style.copyWith(
                fontSize: 40,
                fontWeight: FontWeight.w300
            )
        )
    );

    final appLoginTitleWrap = SizedBox(
      child: ListView(
        shrinkWrap: true,
        children: <Widget>[appLoginTitleImg, SizedBox(height:10), appLoginTitleText],
      ),
    );

    final email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      initialValue: 'alucard@gmail.com',
      decoration: InputDecoration(
        hintText: 'Email',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final password = TextFormField(
      autofocus: false,
      initialValue: 'some password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.only(top: 15.0, bottom: 5.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () {
          Navigator.of(context).pushNamed(HomePage.tag);
        },
        padding: EdgeInsets.all(12),
        color: MaterialColor(CompanyColors.pink[50].value, CompanyColors.pink),
        child: Text('Log In'.toUpperCase(), style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            appLoginTitleWrap,
            SizedBox(height: 48.0),
            email,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 24.0),
            loginButton,
            forgotLabel
          ],
        ),
      ),
    );
  }
}

TextStyle style = TextStyle(fontFamily: 'Roboto', fontSize: 20.0);

class CompanyColors {
  CompanyColors._(); // this basically makes it so you can instantiate this class
  static const Map<int, Color> pink = const <int, Color>{
    50: const Color(0xffed3e72),
    100: const Color(0xffed3e72),
    200: const Color(0xffed3e72),
    300: const Color(0xffed3e72),
    400: const Color(0xffed3e72),
    500: const Color(0xffed3e72),
    600: const Color(0xffed3e72),
    700: const Color(0xffed3e72),
    800: const Color(0xffed3e72),
    900: const Color(0xffed3e72)
  };
}
